const request = require('request');
const jwtUtil = require('jwt-simple');
var RSVP = require('rsvp');
var _ = require('lodash');

const prettyjson = require('prettyjson');
function prettify_json(data, options = {}) {
  return '{\n' + prettyjson.render(data, options) + '\n}'
}
/**
   * Securing your app with JWT
   * --------------------------
   * Whenever Stride makes a call to your app (webhook, glance, sidebar, bot), it passes a JSON Web Token (JWT).
   * This token contains information about the context of the call (cloudId, conversationId, userId)
   * This token is signed, and you should validate the signature, which guarantees that the call really comes from Stride.
   * You validate the signature using the app's client secret.
   *
   * In this tutorial, the token validation is implemented as an Express middleware function which is executed
   * in the call chain for every request the app receives from Stride.
   * The function extracts the context of the call from the token and adds it to a local variable.
   */
  var debugId = 'stride.js';
  var loadClientInfo = function (settings, clientKey) {
    var self = this;
    return new RSVP.Promise(function (resolve, reject) {
        settings.get('clientInfo', clientKey).then(function (d) {
            resolve(d);
        }, function (err) {
            reject(err);
        });
    });
  };

  function getJWT(req) {
    // Extract the JWT token from the request
    // Either from the "jwt" request parameter
    // Or from the "authorization" header, as "Bearer xxx"
    var encodedJwt = req.query['jwt'] || req.query['signed_request'];
    
    if (!encodedJwt){
      if (req.headers['authorization']){
        encodedJwt = req.headers['authorization'];
        if (encodedJwt.indexOf('JWT ') == 0) encodedJwt = encodedJwt.substring(4);
        else if (encodedJwt.indexOf("Bearer") == 0) encodedJwt = encodedJwt.substring(7);
      }
    }

    if  (!encodedJwt)
      throw new Error('Stride/getJWT: expected encoded JWT not found!')

    // Decode the base64-encoded token, which contains the context of the call
    const decodedJwt = jwtUtil.decode(encodedJwt, null, true)

    const jwt = {encoded: encodedJwt, decoded: decodedJwt}

    console.log(`- ${debugId}/getJWT() got JWT`/*, prettify_json(jwt)*/)

    return jwt
  }

  function validateJWT(settings, req, res, next) {
    let logDetails = {
      debugId,
      endpoint: req.path,
      method: req.method,
    }

    try {
      const jwt = getJWT(req)

      console.log(`- ${debugId}/validating JWT...`)

      // Validate the token signature using the app's OAuth secret (created in DAC App Management)
      // (to ensure the call comes from Stride)
      jwtUtil.decode(jwt.encoded, process.env.CLIENT_SECRET)

      // all good, it's from Stride
      console.info(`- ${debugId}: JWT valid`/*, prettify_json({...logDetails})*/)

      // if any, add the context to a local variable
      const conversationId = jwt.decoded.context.resourceId
      const cloudId = jwt.decoded.context.cloudId
      const userId = jwt.decoded.sub

      logDetails = {
        logDetails,
        cloudId,
        conversationId,
        userId,
      }
      req.identity = _.assign({}, {
        groupId: cloudId,
        roomId: conversationId,
        userId: userId
      });

      req.isStride = true;
      loadClientInfo(settings, cloudId).then(function(clientInfo){
        if (clientInfo === null) {
          return send(400, "App hasn't been installed yet in this instance");
        }
        req.clientInfo = clientInfo;
        next();
      }, function(err){
        console.error(err);
        res.status(400).send('Error getting client info');
      });

      //res.locals.context = {cloudId, conversationId, userId};

      // Continue with the rest of the call chain
     
    } catch (err) {
      console.warn(`! ${debugId}: Invalid JWT:` + err.message, prettify_json({logDetails, err}))
      // a rogue call not frow a legitimate Stride client?
      res.sendStatus(403)
    }
  }

  module.exports = {
      validateJWT
  }